//
//  gameOver.swift
//  assignment2_ridiculous
//
//  Created by Shashank Machani on 2019-10-24.
//  Copyright © 2019 Keerthi. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class GameOver:SKScene {
    var newgame :SKSpriteNode!
    var ngLabel :SKLabelNode!
    var guns: SKSpriteNode!
    var gunLabel :SKLabelNode!
    var score: SKSpriteNode!
    var scoreLabel: SKLabelNode!
    
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMove(to view: SKView) {
        ////        self.physicsWorld.contactDelegate! = self
        //        self.newgame = self.childNode(withName: "newGame") as! SKSpriteNode
        //        self.ngLabel = self.childNode(withName: "ngLabel") as! SKLabelNode
        //        self.guns = self.childNode(withName: "guns") as! SKSpriteNode
        //        self.gunLabel = self.childNode(withName: "gunsLabel") as! SKLabelNode
        //        self.score = self.childNode(withName: "score") as! SKSpriteNode
        //        self.scoreLabel = self.childNode(withName: "scoreLabel") as! SKLabelNode
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let main = GameScene(size:self.size)
        self.view?.presentScene(main)
    }
}
