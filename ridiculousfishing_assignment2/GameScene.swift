//
//  GameScene.swift
//  assignment2_ridiculous
//
//  Created by Shashank Machani on 2019-10-23.
//  Copyright © 2019 Keerthi. All rights reserved.
//

import SpriteKit
import GameplayKit

struct PhysicsCategory
{
    static let Player: UInt32 = 1
    static let Fish: UInt32 = 2
    static let Edge: UInt32 = 4
    
}
class GameScene: SKScene,SKPhysicsContactDelegate {
    
    var bullet :Bullet?
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    
    var cameraNode = SKCameraNode()
    var scoreLabel : SKLabelNode!
    
    var sky: SKSpriteNode!
    var background1: SKSpriteNode!
    var background2: SKSpriteNode!
    var background3: SKSpriteNode!
    var background4: SKSpriteNode!
    var hook :SKSpriteNode!
    var ship :SKSpriteNode!
    var top : SKSpriteNode!
    var bottom: SKSpriteNode!
    
    var fishes:[SKSpriteNode] = []
    var fishescatched:[SKSpriteNode] = []
    var counter = 0
    var score = 0
    let ledge = SKNode()
    
    var gameStarted: Bool = false
    var touchPoint: CGPoint = CGPoint()
    
    var touching: Bool = false
    
    var sound = SKAction.playSoundFileNamed("underwater.wav", waitForCompletion: false)
    override func didMove(to view: SKView)
    {
        self.physicsWorld.contactDelegate = self
        
        self.background1 = self.childNode(withName: "background1") as! SKSpriteNode
        self.background2 = self.childNode(withName: "background2") as! SKSpriteNode
        self.background3 = self.childNode(withName: "background3") as! SKSpriteNode
        self.background4 = self.childNode(withName: "background4") as! SKSpriteNode
        self.ship = self.childNode(withName: "ship") as! SKSpriteNode
        self.hook = self.childNode(withName: "hook") as! SKSpriteNode
        self.top = self.childNode(withName: "topCover") as! SKSpriteNode
       
        self.scoreLabel = SKLabelNode(text: "score:\(score)")
        self.scoreLabel.position = CGPoint(x:0, y:2000)
        self.scoreLabel.fontColor = .black
        self.scoreLabel.fontSize = 80
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.zPosition = 2
        addChild(self.scoreLabel)
        
        hook.physicsBody?.affectedByGravity = false
        hook.physicsBody?.collisionBitMask =  4
        hook.physicsBody?.allowsRotation = false
        hook.physicsBody?.collisionBitMask = PhysicsCategory.Fish
        hook.zPosition = 2
     
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        counter = counter + 1
        
        if(counter%80 == 0){
            spawnFishes()
        }
        
       
        attachfish()
        
    }
    func attachfish(){
        for(index,fish)in fishes.enumerated(){
            
            if (self.hook.intersects(fish) == true){
                fishescatched.append(fish)
                fish.removeAllActions()
                                let joint = SKPhysicsJointPin.joint(withBodyA: hook.physicsBody!, bodyB: fish.physicsBody!,
                                                                    anchor: CGPoint(x: hook.frame.midX, y: fish.frame.midY))
                                physicsWorld.add(joint)
//                fish.position.x = hook.position.x
//                fish.position.y = hook.position.y
                var reelHook = SKAction.moveTo(y: 1000, duration: Double(10))
                
                hook?.run(reelHook)
                fish.run(reelHook)

                
                if (hook.position.y >= 600){
                    self.hook.isHidden = true
                    physicsWorld.removeAllJoints()
                    self.hook.removeAllActions()
                    self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
                    let randomXPos = Int.random(in: 100..<1500)
                    let randomYPos = Int.random(in: 1500..<2000)
                    let flyup = SKAction.move(to: CGPoint(x: randomXPos, y: randomYPos),
                                              duration: 8)
                    let falldown = SKAction.move(to: CGPoint(x: fish.position.x, y: -1000), duration: 8)
                    let fishAnimation = SKAction.sequence([flyup,falldown,SKAction.run({
                        () -> Void in
                        let finishScene = GameOver(size: self.size)
                        self.view?.presentScene(finishScene, transition: SKTransition.doorsCloseVertical(withDuration:1.5))
                    })])
                    
                    fish.run(fishAnimation)
                    
                }
            }
        }
        
    }
    
    func spawnFishes(){
        var fish: SKSpriteNode?
        let randomNumber  = Int.random(in: 0..<5)
        if randomNumber == 0
        {
            fish = SKSpriteNode(imageNamed: "fish1")
        }
        else if randomNumber == 1
        {
            fish = SKSpriteNode(imageNamed: "fish2")
        }
        else if randomNumber == 2
        {
            fish = SKSpriteNode(imageNamed: "fish3")
            
        }
        else if randomNumber == 3
        {
            fish = SKSpriteNode(imageNamed: "crab")
        }
        else if randomNumber == 4
        {
            fish = SKSpriteNode(imageNamed: "dolphin")
        }
        else
        {
            fish = SKSpriteNode(imageNamed: "style2_4")
        }
        let randomXPos = Int.random(in: 100..<1000)
        let randomYPos = Int.random(in: -2045..<700)
        
        fish!.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 350, height: 250))
        fish!.physicsBody?.affectedByGravity = false
        fish!.physicsBody?.allowsRotation  = false
        fish!.position = CGPoint(x:randomXPos, y:randomYPos)
        fish!.zPosition = 1
        fish!.size = CGSize(width: 350, height: 250)
        addChild(fish!)
        self.fishes.append(fish!)
        
        let move1 = SKAction.move(to: CGPoint(x: 1000, y: (fish?.position.y)!),
                                  duration: 10)
        
        let move2 = SKAction.move(to: CGPoint(x: -960 , y: (fish?.position.y)!),
                                  duration: 10)
        let fishAnimation = SKAction.sequence([move1,move2])
        
        let fishForeverAnimation = SKAction.repeatForever(fishAnimation)
        fish!.run(fishForeverAnimation)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let locationTouched = touches.first
        let positionInScene = locationTouched!.location(in:self)
        
        let touchedNode = self.atPoint( positionInScene)
        
        print(fishescatched.count)
        
        if touchedNode.name == "ship" {
            self.bullet = Bullet()
            bullet?.position = positionInScene
            bullet?.name = "bullet"
            addChild(self.bullet!)
            self.bullet?.physicsBody?.isDynamic = false
            
            // set the starting position of the finger
            self.mouseStartingPosition = positionInScene
        }
        
        
        if touchedNode.name == "hook" {
            self.gameStarted = true
            var dropHook = SKAction.moveTo(y:-2000, duration: Double(15))
            hook?.run(dropHook)
            playSound(sound: sound)
        }
        if(hook.position.y <= -2000){
            var reelHook = SKAction.moveTo(y: 1000, duration: Double(6))
            hook?.run(reelHook)
            
        }
        if (hook.position.y > ship.position.y){
            hook.removeAllActions()
            hook.isHidden = true
        }
        
        
        if positionInScene.y < ship.position.y/4{
            self.hook.position.x = positionInScene.x
            
        }
        
        if positionInScene.y > ship.position.y/4{
            
            for (index,fish) in fishescatched.enumerated(){
                
                for i in 1...counter{
                    
                    if  (touchedNode.name == "\(index)"){
                        print("hello")
                        fishescatched[index].removeFromParent()
                        
                    }
                }
            }
        }
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        //        print("Finger ending position: \(mouseLocation)")
        
        // 1. get the ending position of the finger
        let bulletEndingPosition = mouseLocation
        
        // 2. get the difference between finger start & end
        let diffX = bulletEndingPosition.x - self.mouseStartingPosition.x
        let diffY = bulletEndingPosition.y - self.mouseStartingPosition.y
        
        // 3. throw the bullet based on that direction
        let direction = CGVector(dx: diffX, dy: diffY)
        self.bullet?.physicsBody?.isDynamic = true
        self.bullet?.physicsBody?.applyImpulse(direction)
        
        
    }
    func playSound(sound : SKAction)
    {
        run(sound)
    }
    
    //    func didBegin(_ contact: SKPhysicsContact) {
    //
    //
    //        let nodeA = contact.bodyA.node
    //        let nodeB = contact.bodyB.node
    //        for(index,fish) in  fishescatched.enumerated(){
    //            print(fish)
    //
    //        }
    //        if (contact.collisionImpulse > 40) {
    //            if (nodeA?.name == "") {
    //                print("Skull hit: \(nodeB?.name)")
    //                print("Impact amount: \(contact.collisionImpulse)")
    //                print("--------")
    //
    //
    //                // animate and remove the skull
    //                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
    //                let removeNode = SKAction.removeFromParent()
    //
    //                let sequence = SKAction.sequence([reduceImageSize, removeNode])
    //
    //                nodeA?.run(sequence)
    //
    //            }
    //            else if (nodeB?.name == "skull") {
    //                print("Skull hit: \(nodeA?.name)")
    //                print("Impact amount: \(contact.collisionImpulse)")
    //                print("--------")
    //
    //                // animate and remove the skull
    //                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
    //                let removeNode = SKAction.removeFromParent()
    //
    //                let sequence = SKAction.sequence([reduceImageSize, removeNode])
    //
    //                nodeB?.run(sequence)
    //
    //            }
    //        }
    //    }
    
    
}
